import turtle
import random
from random import randint
import math
from matplotlib import pyplot as plt
import scipy
from scipy import constants
gr=scipy.constants.golden


def defineColours():
    mjc = {'brl':
           {'mg': '#E000D8', 'pk': '#FF00A3', 'rd': '#FF0053',
            'orng': '#FF6822', 'yl': '#FFDF00', 'grn': '#70D900',
            'trq': '#00dbf2', 'bl': '#0073FF', 'ind': '#3600F0',
            'vlt': '#7C00FF', 'prp': '#BB00FF', 'gry': '#9E9E9E'
            },

           'swt':
            {'mg': '#FF96F3', 'pk': '#FF8CBA', 'rd': '#FF7A8A',
             'orng': '#FF8C4E', 'yl': '#FFF29F', 'grn': '#B9FF71',
             'trq': '#97F5E7', 'bl': '#30A9FF', 'ind': '#597EFF',
             'vlt': '#8A80FF', 'prp': '#CE90FF', 'gry': '#BFBFBF'
             },

           'pale':
            {'mg': '#FBC9FA', 'pk': '#FFCADC', 'rd': '#FFBBC1',
             'orng': '#FFC9B1', 'yl': '#FFFFC9', 'grn': '#E0FFB8',
             'trq': '#D1FFEC', 'bl': '#C9F1FE', 'ind': '#C7DAFD',
             'vlt': '#C8C4FD', 'prp': '#E5CDFD', 'gry': '#E3E3E3'
             },

           'dim':
            {'mg': '#BD84AE', 'pk': '#C07C9B', 'rd': '#C26D70',
             'orng': '#BD7049', 'yl': '#E3D0A3', 'grn': '#8FB27D',
             'trq': '#A9D9D2', 'bl': '#83B5D6', 'ind': '#7490D6',
             'vlt': '#787ABF', 'prp': '#AE91C2', 'gry': '#8A8A8A'
             },

           'drk':
            {'mg': '#89037D', 'pk': '#940358', 'rd': '#870014',
             'orng': '#6B2D00', 'yl': '#A37E02', 'grn': '#005700',
             'trq': '#005B69', 'bl': '#004280', 'ind': '#000091',
             'vlt': '#42008A', 'prp': '#550066', 'gry': '#303030'
             }
          }

    palser = ['brl', 'swt', 'pale', 'dim', 'drk']
    colrs = ['mg', 'pk', 'rd', 'orng', 'yl', 'grn',
             'trq', 'bl', 'ind', 'vlt', 'prp', 'gry']
    palpos = [list(zip([x+0.5 for i in range(len(colrs))], 
                       range(len(colrs)))) 
              for x in range(len(palser))]
    palcol = [[mjc[i][j] for j in colrs] for i in palser]

    colfig = plt.figure(figsize = (8,4))
    return mjc
    
def drawCircles(t, size, xcor, ycor, xshift, yshift, angle, degrees,layer_width):
  if size < 1 or degrees >= 360:
      return
      
  t.penup()
  t.setx(xcor+xshift)
  t.sety(ycor+yshift)
  t.left(angle)
  t.pendown()

  if t.fillcolor() == "blue":
      t.fillcolor("purple")
  else:
      t.fillcolor("blue")  
  
  t.begin_fill()
  t.circle(size)
  t.end_fill()

  drawCircles(t, size-layer_width, t.xcor(), t.ycor(), xshift, yshift, angle,degrees+angle,layer_width)

def initialiseTurtle():
    screen = turtle.Screen()
    screen.bgcolor("black") #black background
    screen.colormode(2)
    t = turtle.Turtle()
    defineColours()
    turtle.tracer(0, 0)    #speedie turtle
    t.ht()
    t.penup()
    t.setx(0)
    t.sety(0)
    return t


def drawPetals(t, size, petals):
    for i in range(petals):
        drawCircles(t, size, 0, 0, 0, 0, 2,0) #up
        
def drawSymPetals(t, size, petals, layer_width):
    print("angle = ",360/(size/layer_width))
    for i in range(petals):
        #t.seth(360/petals*i)
        t.left(360/petals)
        drawCircles(t, size, 0, 0, 0, 0, 360/(size/layer_width),0,layer_width) #up

def drawFlowers(t, size, depth, petals, layer_width):
    if depth < 1:
        return
    o = t.heading()
    offset = (math.radians(gr))*360+o
    print("offset", offset, "heading:", t.heading())
    t.left(offset)
    
    drawSymPetals(t, size, petals, layer_width)
    #2 types of petals
    #drawPetals(t, size, petals)
    drawSymPetals(t, size, petals, layer_width)
    drawFlowers(t, size-(size/depth*0.7), depth-1, petals, layer_width)
    #drawFlowers(t, size-(size/depth), depth-1, petals, layer_width)

#recursive petals
def main():
    t = initialiseTurtle()
    drawFlowers(t=t,size=120, depth=3,petals=3, layer_width=1)
    #drawFlowers(t=t,size=300,petals=5,depth=1)
    
    turtle.done()
    turtle.bye()

    #cool commands - normal petals
    #drawFlowers(t=t,size=250,petals=10,depth=5)
    #drawFlowers(t=t,size=150,petals=4,depth=30)

main()