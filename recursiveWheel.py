import turtle
import random
from random import randint
import math
from matplotlib import pyplot as plt
import scipy
from scipy import constants
gr=scipy.constants.golden

def initializeTurtle():
    screen = turtle.Screen()
    screen.bgcolor("black") #black background
    screen.colormode(2)
    t = turtle.Turtle()
    defineColours()
    turtle.tracer(0, 0)    #speedy turtle
    t.speed(0)
    t.ht()
    t.penup()
    t.setx(0)
    t.sety(-320)
    return t

def defineColours():
    mjc = {'brl':
           {'mg': '#E000D8', 'pk': '#FF00A3', 'rd': '#FF0053',
            'orng': '#FF6822', 'yl': '#FFDF00', 'grn': '#70D900',
            'trq': '#00dbf2', 'bl': '#0073FF', 'ind': '#3600F0',
            'vlt': '#7C00FF', 'prp': '#BB00FF', 'gry': '#9E9E9E'
            },

           'swt':
            {'mg': '#FF96F3', 'pk': '#FF8CBA', 'rd': '#FF7A8A',
             'orng': '#FF8C4E', 'yl': '#FFF29F', 'grn': '#B9FF71',
             'trq': '#97F5E7', 'bl': '#30A9FF', 'ind': '#597EFF',
             'vlt': '#8A80FF', 'prp': '#CE90FF', 'gry': '#BFBFBF'
             },

           'pale':
            {'mg': '#FBC9FA', 'pk': '#FFCADC', 'rd': '#FFBBC1',
             'orng': '#FFC9B1', 'yl': '#FFFFC9', 'grn': '#E0FFB8',
             'trq': '#D1FFEC', 'bl': '#C9F1FE', 'ind': '#C7DAFD',
             'vlt': '#C8C4FD', 'prp': '#E5CDFD', 'gry': '#E3E3E3'
             },

           'dim':
            {'mg': '#BD84AE', 'pk': '#C07C9B', 'rd': '#C26D70',
             'orng': '#BD7049', 'yl': '#E3D0A3', 'grn': '#8FB27D',
             'trq': '#A9D9D2', 'bl': '#83B5D6', 'ind': '#7490D6',
             'vlt': '#787ABF', 'prp': '#AE91C2', 'gry': '#8A8A8A'
             },

           'drk':
            {'mg': '#89037D', 'pk': '#940358', 'rd': '#870014',
             'orng': '#6B2D00', 'yl': '#A37E02', 'grn': '#005700',
             'trq': '#005B69', 'bl': '#004280', 'ind': '#000091',
             'vlt': '#42008A', 'prp': '#550066', 'gry': '#303030'
             }
          }

    palser = ['brl', 'swt', 'pale', 'dim', 'drk']
    colrs = ['mg', 'pk', 'rd', 'orng', 'yl', 'grn',
             'trq', 'bl', 'ind', 'vlt', 'prp', 'gry']
    palpos = [list(zip([x+0.5 for i in range(len(colrs))], 
                       range(len(colrs)))) 
              for x in range(len(palser))]
    palcol = [[mjc[i][j] for j in colrs] for i in palser]
    colfig = plt.figure(figsize = (8,4))
    return mjc

def drawSemi(t,size, line_length, line_angle):
    t.pendown()
    mjc=defineColours()
    colours=list(mjc['brl'].values())[:-1]
    t.pencolor(colours[randint(0,len(colours)-1)])
       
    for i in range(0,line_length-1):
        if size > 0 and (i > line_length/6):
            print("size ", size, "i ",i)
            size=size-size/4
        if i % 2 == 0:
            t.circle(size,line_angle)
        else:
            t.circle(-size,line_angle)
        
            
    if line_length % 2 == 1:
        t.circle(-size,line_angle)
    else:
        t.circle(size,line_angle)

def drawLines(t,size,line_length,number_lines, line_angle, xcor,ycor):
    if number_lines < 0:
        return
    t.ht()
    t.pu()
    t.setx(xcor)
    t.sety(ycor)
    drawSemi(t,size,line_length,line_angle)
    drawLines(t,size,line_length,number_lines-1,line_angle,xcor+10,ycor+5)
    
def drawClines(t,size, line_length, line_angle, angle, degrees,xcorr,ycorr):
    if degrees >= (360+angle):
        #print(t.heading())
        return
    #print(degrees)
    t.pu()
    t.setx(xcorr)
    t.sety(ycorr)
    t.seth(degrees)
    degrees=degrees+angle
    drawSemi(t,size,line_length,line_angle)
    drawClines(t,size, line_length, line_angle, angle, degrees,xcorr,ycorr)
    
def drawFlower(t,size, line_length, angle,xcorr, ycorr):
    t.pu()
    t.setx(0)
    t.sety(0)
    f=size*line_length/3
    drawClines(t,size, line_length, 180, angle, 0, xcorr-f,ycorr+0)


# colourful wheel
def main():
    t=initializeTurtle()
    t.ht()
    #drawLines(t=t,size=10, line_length=60, number_lines=1,line_angle=180,xcor=-320,ycor=-320)
    #drawClines(t=t,size=10, line_length=10, line_angle=180, angle=2, degrees=0, xcorr=0, ycorr=0)
    drawFlower(t=t,size=20, line_length=20, angle=5, xcorr=0, ycorr=0)

    turtle.done()
    turtle.bye()
main()